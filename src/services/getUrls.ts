import { type DynamoScanResponse } from 'src/types'

export const getUrls = async (): Promise<DynamoScanResponse> => {
    const resp = await fetch('./api/items')
    return resp.json()
}
