export const deleteUrl = async (
    id: string,
    token: string
): Promise<void> => {
    const url = './api/items/${id}'
    await fetch(url, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json',
            'Cookie': `session-auth=${token}`
        },
    })
}
