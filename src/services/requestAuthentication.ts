import { AuthFlowType, CognitoIdentityProviderClient, InitiateAuthCommand } from '@aws-sdk/client-cognito-identity-provider'

export const requestAuthentication = async (username: string, password: string): Promise<string> => {
    const client = new CognitoIdentityProviderClient({
        region: process.env.AWS_REGION,
    })

    const command = new InitiateAuthCommand({
        AuthFlow: AuthFlowType.USER_PASSWORD_AUTH,
        AuthParameters: {
            USERNAME: username,
            PASSWORD: password,
        },
        ClientId: process.env.USER_POOL_CLIENT_ID,
    })

    const data = await client.send(command)
    const token = data.AuthenticationResult?.AccessToken
    if (!token) {
        throw new Error('No token found')
    }
    return token
}