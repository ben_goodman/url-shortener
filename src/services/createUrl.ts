export const createUrl = async (
    id: string,
    targetUrl: string,
    token: string
): Promise<void> => {
    const url = './api/items'
    await fetch(url, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'Cookie': `session-auth=${token}`
        },
        body: JSON.stringify({
            id,
            targetUrl
        })
    })
}
