import { CognitoIdentityProvider } from '@aws-sdk/client-cognito-identity-provider'

export const revokeAuthentication = async (token: string) => {

    const client = new CognitoIdentityProvider({
        region: process.env.AWS_REGION,
    })

    return client.globalSignOut({AccessToken: token})
}
