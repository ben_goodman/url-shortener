import React, { useState } from 'react'
import { FormControl, TextInput } from '@contentful/f36-forms'
import { Form } from 'react-router-dom'
import { requestAuthentication } from 'src/services/requestAuthentication'
import { Box } from '@contentful/f36-core'
import { Notification } from '@contentful/f36-notification'
import { StyledButton } from 'src/components/StyledButton'

export interface LoginFormProps {
    onSuccess?: (token: string) => void,
    onError?: () => void
}

export const LoginForm = ({
    onSuccess,
    onError
}: LoginFormProps) => {
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const [isSubmitDisabled, setSubmitDisabled] = useState(false)

    const handleSubmit = () => {
        setSubmitDisabled(true)
        requestAuthentication(username, password)
            .then((token) => {
                Notification.success('Login successful')
                onSuccess?.(token)
            })
            .catch((error) => {
                console.log(error)
                Notification.error('Incorrect username or password')
                onError?.()
            })
            .finally(() => {
                setSubmitDisabled(false)
            })
    }

    const handleInput = (e: React.ChangeEvent<HTMLInputElement>) => {
        const {name, value} = e.target
        if (name === 'username') {
            setUsername(value)
        } else if (name === 'password') {
            setPassword(value)
        } else {
            throw new Error(`Unknown input name: ${name}`)
        }
    }

    return (
        <Box>
            <Form>
                <FormControl>
                    <FormControl.Label isRequired>Username</FormControl.Label>
                    <TextInput name='username' value={username} onChange={handleInput}/>
                </FormControl>

                <FormControl>
                    <FormControl.Label isRequired>Password</FormControl.Label>
                    <TextInput type='password' name='password' value={password} onChange={handleInput}/>
                </FormControl>

                <StyledButton
                    variant="primary"
                    isDisabled={isSubmitDisabled}
                    onClick={handleSubmit}
                >
                    Login
                </StyledButton>
            </Form>
        </Box>
    )

}