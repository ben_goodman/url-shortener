import { Table } from '@contentful/f36-table'
import styled from 'styled-components'

export const StyledTable = styled(Table)`
    margin-bottom: 1rem !important;
    border-radius: ${props => props.theme.articlePage.body.table.border.radius} !important;
`

export const BodyTableHeaderCell = styled(Table.Cell)`
    background-color: ${props => props.theme._rootTheme.tokens.color.cyan} !important;
`

export const BodyTableCell = styled(Table.Cell)`
    background-color: ${props => props.theme._rootTheme.tokens.color.lightBlack} !important;
    * {
        font-family: ${props => props.theme.articlePage.body.table.cell.font.family} !important;
        color: ${props => props.theme.articlePage.body.table.cell.color} !important;
    }
`