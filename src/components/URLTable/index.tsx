import React from 'react'
import { Table } from '@contentful/f36-table'
import { type DynamoScanResponse } from 'src/types'
import { TextLink } from '@contentful/f36-text-link'
import { IconButton } from '@contentful/f36-button'
import { DateTime } from '@contentful/f36-datetime'
import { DeleteIcon } from '@contentful/f36-icons'
import { BodyTableHeaderCell, StyledTable, BodyTableCell } from './styles'
import styled from 'styled-components'


const StyledDeleteButton = styled(IconButton)`
    background: ${props => props.theme._rootTheme.tokens.color.link} !important;
`

export interface UrlTableProps {
    entries?: DynamoScanResponse['Items'],
    isUserAuthenticated: boolean,
    onItemDelete: (shortPath: string) => void,
}

export const URLTable = ({
    entries,
    isUserAuthenticated = false,
    onItemDelete,
}: UrlTableProps) => {

    return (
        <StyledTable>
            <Table.Head>
                <Table.Row>
                    <BodyTableHeaderCell>Short Url</BodyTableHeaderCell>
                    <BodyTableHeaderCell>Long Url</BodyTableHeaderCell>
                    <BodyTableHeaderCell>Created At</BodyTableHeaderCell>
                    {isUserAuthenticated && <BodyTableHeaderCell>Delete</BodyTableHeaderCell>}
                </Table.Row>
            </Table.Head>
            <Table.Body>
                {
                    entries?.map((item) => {
                        return (
                            <Table.Row key={item.id.S}>
                                <BodyTableCell>
                                    <TextLink
                                        href={`./${item.id.S}`}
                                        target='_blank'
                                    >
                                        /{item.id.S}
                                    </TextLink>
                                </BodyTableCell>
                                <BodyTableCell>
                                    <TextLink
                                        href={item.targetUrl?.S}
                                        target='_blank'
                                    >
                                        {item.targetUrl?.S}
                                    </TextLink>
                                </BodyTableCell>
                                <BodyTableCell>
                                    <DateTime
                                        date={item.dateUploaded.S}
                                    />
                                </BodyTableCell>
                                {isUserAuthenticated && <BodyTableCell>
                                    <StyledDeleteButton
                                        onClick={() => onItemDelete(item.id.S)}
                                        aria-label='Delete'
                                        variant='secondary'
                                        icon={<DeleteIcon/>}
                                        isDisabled={!isUserAuthenticated}
                                    />
                                </BodyTableCell>}
                            </Table.Row>
                        )
                    })
                }
            </Table.Body>
        </StyledTable>
    )
}