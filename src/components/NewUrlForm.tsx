import { Button } from '@contentful/f36-button'
import { Box } from '@contentful/f36-core'
import { Form, FormControl, TextInput } from '@contentful/f36-forms'
import { Notification } from '@contentful/f36-notification'
import React, { useEffect, useState } from 'react'
import { useCookies } from 'react-cookie'
import { createUrl } from 'src/services/createUrl'

const SESSION_AUTH_COOKIE = 'session-auth'

export interface NewUrlFormProps {
    onSuccess?: () => void,
    onError?: () => void
}

export const NewUrlForm = ({
    onSuccess,
    onError,
}: NewUrlFormProps) => {
    const [authCookie, ] = useCookies([SESSION_AUTH_COOKIE])

    const [shortPath, setShortPath] = useState('')
    const [targetUrl, setTargetUrl] = useState('')
    const [isSubmitDisabled, setSubmitDisabled] = useState(false)
    const [isFormInvalid, setIsFormInvalid] = useState(false)

    const handleInput = (e: React.ChangeEvent<HTMLInputElement>) => {
        const {name, value} = e.target
        if (name === 'shortPath') {
            setShortPath(value)
        } else if (name === 'targetUrl') {
            setTargetUrl(value)
        } else {
            throw new Error(`Unknown input name: ${name}`)
        }
    }

    const handleSubmit = () => {
        setSubmitDisabled(true)
        // get session auth token
        const token = authCookie[SESSION_AUTH_COOKIE]
        createUrl(shortPath, targetUrl, token)
            .then(() => {
                Notification.success('Url created')
                onSuccess?.()
            })
            .catch((error) => {
                console.log(error)
                Notification.error('Url creation failed')
                onError?.()
            })
            .finally(() => {
                setSubmitDisabled(false)
            })
    }

    useEffect(() => {
        const isInputValid =  /^[A-z0-9]+$/.test(shortPath)
        setIsFormInvalid(!isInputValid)
    }, [shortPath])


    return (
        <Box>
            <Form>
                <FormControl>
                    <FormControl.Label isRequired>Short Path</FormControl.Label>
                    <TextInput
                        name='shortPath'
                        value={shortPath}
                        onChange={handleInput}
                        isInvalid={isFormInvalid}
                    />
                    { isFormInvalid && <FormControl.HelpText>Path must not contain special characters or spaces.</FormControl.HelpText> }
                </FormControl>

                <FormControl>
                    <FormControl.Label isRequired>Target URL</FormControl.Label>
                    <TextInput name='targetUrl' value={targetUrl} onChange={handleInput}/>
                    <FormControl.HelpText>The full URL to redirect to.</FormControl.HelpText>
                </FormControl>

                <Button
                    variant='primary'
                    isDisabled={isSubmitDisabled || isFormInvalid}
                    onClick={handleSubmit}
                >
                    Create
                </Button>
            </Form>
        </Box>
    )
}