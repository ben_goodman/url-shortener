import { Button } from '@contentful/f36-button'
import styled from 'styled-components'

export const StyledButton = styled(Button)`
    background-color: ${props => props.theme.button._variant.default.backgroundColor} !important;
    color: ${props => props.theme.button._variant.default.color} !important;
    border: unset !important;
    border-radius: 0px important!;
    padding: 0.75rem;
    cursor: pointer;

    &:hover {
        text-decoration: underline;
    }

    &[data-is-disabled="true"] {
        cursor: initial !important;
        text-decoration: line-through;
        background-color: ${props => props.theme.button._variant.disabled.backgroundColor} !important;
    }
`