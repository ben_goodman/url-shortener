import React, { useEffect, useState } from 'react'
import { Modal } from '@contentful/f36-modal'
import { LoginForm } from 'src/components/LoginForm'
import { URLTable } from 'src/components/URLTable'
import { Header } from '@contentful/f36-header'
import { Box, Flex } from '@contentful/f36-core'
import { Notification } from '@contentful/f36-notification'
import tokens from '@contentful/f36-tokens'
import { useCookies } from 'react-cookie'
import { revokeAuthentication } from 'src/services/revokeAuthentication'
import { deleteUrl } from 'src/services/deleteUrl'
import { NewUrlForm } from 'src/components/NewUrlForm'
import { DynamoScanResponse } from 'src/types'
import { getUrls } from 'src/services/getUrls'
import { StyledButton } from 'src/components/StyledButton'
import styled from 'styled-components'

const SESSION_AUTH_COOKIE = 'session-auth'

const StyledHeader = styled(Header)`
    background-color: ${props => props.theme._rootTheme.tokens.color.cyan} !important;
    // color: ${props => props.theme.colorTextPrimary};
`

export const Component = () => {
    const [urlEntries, setUrlEntries] = useState<DynamoScanResponse>()
    const [authCookie, setCookie, removeCookie] = useCookies([SESSION_AUTH_COOKIE])
    const [isLoginModalOpen, setLoginModalOpen] = useState(false)
    const [isNewUrlModalOpen, setNewUrlModalOpen] = useState(false)


    const fetchEntries = () => {
        getUrls().then((response) => {
            setUrlEntries(response)
        })
    }

    const handleAuthSuccess = (token: string) => {
        setCookie(
            SESSION_AUTH_COOKIE,
            token,
            {
                expires: new Date(Date.now() + (3600 * 1000)),
                sameSite: 'strict',
            }
        )
        setLoginModalOpen(false)
    }

    const handleLogoutClick = () => {
        revokeAuthentication(authCookie[SESSION_AUTH_COOKIE]).then(() => {
            removeCookie(SESSION_AUTH_COOKIE)
        }).catch(() => {
            Notification.error('Logout error')
        }).then(() => {
            Notification.success('Logout successful')
        })
    }

    const handleUrlDelete = (shortPath: string) => {
        deleteUrl(shortPath, authCookie[SESSION_AUTH_COOKIE])
            .then(() => {
                fetchEntries()
            })
    }

    const handleNewUrlClick = () => {
        setNewUrlModalOpen(true)
    }

    const handleCreationSuccess = () => {
        setNewUrlModalOpen(false)
        fetchEntries()
    }

    useEffect(() => {
        void fetchEntries()
    }, [])

    return (
        <>
            <StyledHeader
                title="Unimportant Links"
                actions={
                    <Flex
                        alignItems="center"
                        gap={tokens.spacingS}
                        justifyContent="flex-end"
                    >
                        {!authCookie[SESSION_AUTH_COOKIE] ?
                            <StyledButton
                                onClick={() => setLoginModalOpen(true)}
                                variant="secondary"
                                size="small"
                                isDisabled={!!authCookie[SESSION_AUTH_COOKIE]}
                            >
                                Login
                            </StyledButton>
                            :
                            <>
                                <StyledButton
                                    variant='primary'
                                    onClick={handleNewUrlClick}
                                    size="small"
                                >
                                    New URL
                                </StyledButton>
                                <StyledButton
                                    variant="secondary"
                                    onClick={handleLogoutClick}
                                    size="small"
                                >
                                    Logout
                                </StyledButton>

                            </>
                        }
                    </Flex>
                }
            />
            <Modal onClose={() => setLoginModalOpen(false)} isShown={isLoginModalOpen}>
                {() => (
                    <>
                        <Modal.Header
                            title="Login"
                            onClose={() => setLoginModalOpen(false)}
                        />
                        <Modal.Content>
                            <LoginForm
                                onSuccess={handleAuthSuccess}
                            />
                        </Modal.Content>
                    </>
                )}
            </Modal>
            <Modal onClose={() => setNewUrlModalOpen(false)} isShown={isNewUrlModalOpen}>
                {() => (
                    <>
                        <Modal.Header
                            title="Add URL"
                            onClose={() => setNewUrlModalOpen(false)}
                        />
                        <Modal.Content>
                            <NewUrlForm
                                onSuccess={handleCreationSuccess}
                            />
                        </Modal.Content>
                    </>
                )}
            </Modal>

            <Box
                padding="spacingXl"
            >
                <URLTable
                    entries={urlEntries?.Items}
                    isUserAuthenticated={!!authCookie[SESSION_AUTH_COOKIE]}
                    onItemDelete={handleUrlDelete}
                />

            </Box>
        </>
    )
}

Component.displayName = 'Home'
