export interface DynamoString {
    S: string
}

export interface DynamoNumber {
    N: number
}

export interface DynamoList<T> {
    L: Array<T>
}

export interface DynamoScanResponse {
    $metadata: {
        httpStatusCode: number,
        requestId: string,
        extendedRequestId: string|undefined,
        cfId: string|undefined,
        attempts: number,
        totalRetryDelay: number,
    }
    Items: Array<UrlEntry>
    Count: number
    ScannedCount: number
    LastEvaluatedKey?: {id: DynamoString}
}

export interface UrlEntry {
    targetUrl: DynamoString
    id: DynamoString
    dateUploaded: DynamoString
}

export interface UrlItem {
    targetUrl: string
    id: string
    dateUploaded: string
}