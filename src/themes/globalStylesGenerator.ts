import { DefaultTheme } from 'styled-components'
import { RootTheme } from './rootThemes'
import f36Tokens from '@contentful/f36-tokens'



export const expandedStylesGenerator = (
    rootTheme: RootTheme
): DefaultTheme => ({
    _rootTheme: rootTheme,
    global: {
        backgroundColor: rootTheme.global.color,
        textColor: rootTheme.displayText.color,
    },
    mermaidDiagram: {
        ...rootTheme.mermaidDiagram,
    },
    highlightText: {
        ...rootTheme.highlightText,
        borderWidth: '1px',
        padding: '0 3px 0 3px',
    },
    tertiaryText: {
        ...rootTheme.tertiaryText
    },
    caption: {
        font: {
            family: rootTheme.captionText.fontFamily,
            weight: rootTheme.captionText.fontWeight,
            size: rootTheme.captionText.fontSize,
        },
        color: rootTheme.captionText.color,
        border: {
            color: rootTheme.captionText.borderColor,
            style: rootTheme.captionText.borderStyle,
        },
    },
    button: {
        font: {
            family: rootTheme.displayText.fontFamily,
            weight: rootTheme.secondaryText.fontWeight,
        },
        _variant: {
            default: {
                backgroundColor: rootTheme.tokens.color.link,
                color: f36Tokens.gray900
            },
            disabled: {
                backgroundColor: f36Tokens.gray400,
                color: f36Tokens.gray100
            }
        },
    },
    fancyLink: {
        font: {
            family: rootTheme.tertiaryText.fontFamily,
            weight: rootTheme.tertiaryText.fontWeight,
        },
        icon: {
            width: '48px',
            height: '48px',
        },
        label: {
            color: rootTheme.tertiaryText.color,
            backgroundColor: rootTheme.tokens.color.link,
            opacity: 1,
        },
    },
    siteHeader: {
        title: {
            font: {
                family: rootTheme.displayText.fontFamily,
                weight: rootTheme.secondaryText.fontWeight,
                size: '2.5rem',
            },
            color: rootTheme.displayText.color
        },
        fancyLink: {
            font: {
                family: rootTheme.displayText.fontFamily,
                weight: rootTheme.secondaryText.fontWeight,
            },
            label: {
                color: f36Tokens.gray200,
                backgroundColor: rootTheme.secondaryText.color,
                opacity: 1,
            },
        },
    },
    articleTopic: {
        font: {
            size: 'inherit',
            family: rootTheme.tertiaryText.fontFamily,
            weight: rootTheme.tertiaryText.fontWeight,
        },
        border: {
            color: rootTheme.tertiaryText.color,
            style: 'solid',
            width: '1px',
        },
        _hover: {
            border: {
                color: f36Tokens.gray900,
            },
        },
        _variant: {
            default: {
                color: rootTheme.tertiaryText.color,
                backgroundColor: rootTheme.tertiaryText.backgroundColor,
            },
            highlighted: {
                color: rootTheme.highlightText.color,
                backgroundColor: rootTheme.highlightText.backgroundColor,
            },
            disabled: {
                // color: rootTheme.disableText.color,
                // backgroundColor: rootTheme.disableText.backgroundColor,
                color: rootTheme.tertiaryText.color,
                backgroundColor: rootTheme.tertiaryText.backgroundColor,
            },
        }
    },
    search: {
        input: {
            font: {
                family: rootTheme.displayText.fontFamily,
            },
            border: {
                width: '1px',
                style: 'solid',
                color: f36Tokens.gray400,
                radius: 0,
            },
            backgroundColor: f36Tokens.gray300,
        },
        resultItem: {
            font: {
                family: rootTheme.paragraphText.fontFamily,
            },
            opacity: 1,
            _hover: {
                backgroundColor: f36Tokens.gray200,
            },
            _disabled: {
                backgroundColor: f36Tokens.gray300,
            },
            textHighlight: {
                border: {
                    radius: rootTheme.highlightText.borderRadius,
                    width: 'inherit',
                    style: rootTheme.highlightText.borderStyle,
                    color: rootTheme.highlightText.borderColor,
                },
                backgroundColor: '#222222',
                color: rootTheme.highlightText.color,
                padding: '0 3px 0 3px',
            }
        },
    },
    articlePreview: {
        card: {
            border: {
                width: '2px',
                radius: 0,
                color: f36Tokens.gray500,
                style: 'solid',
            },
        },
        title: {
            font: {
                family: rootTheme.displayText.fontFamily,
                weight: rootTheme.secondaryText.fontWeight,
                size: f36Tokens.fontSize3Xl,
            },
            color: rootTheme.displayText.color,
            opacity: 1,

        },
        subtitle: {
            font: {
                family: rootTheme.displayText.fontFamily,
                weight: rootTheme.paragraphText.fontWeight,
                size: f36Tokens.fontSizeL,
            },
            opacity: 1,
            color: rootTheme.secondaryText.color,
        }
    },
    articlePage: {
        banner: {
            minHeight: '20rem',
            backgroundColor: rootTheme.global.color,
            font: {
                family: rootTheme.displayText.fontFamily,
                weight: rootTheme.displayText.fontWeight,
            },
            title: {
                font: {
                    size: '5rem',
                },
                lineHeight: '3.8rem',
                color: rootTheme.displayText.color,
                opacity: 1,
            },
            subtitle: {
                font: {
                    weight: rootTheme.secondaryText.fontWeight,
                    size: f36Tokens.fontSizeL,
                },
                lineHeight: f36Tokens.lineHeightL,
                color: rootTheme.secondaryText.color,
                opacity: 1,
            }
        },
        body: {
            maxWidth: rootTheme.global.articleMaxWidth,
            paragraph: {
                font: {
                    family: rootTheme.paragraphText.fontFamily,
                    weight: rootTheme.paragraphText.fontWeight,
                    size: rootTheme.paragraphText.fontSize,
                },
                lineHeight: rootTheme.paragraphText.lineHeight,
                color: rootTheme.paragraphText.color,
                opacity: 1,
            },
            li: {
                marker: {
                    color: rootTheme.secondaryText.color,
                    content: '■ '
                }
            },
            code: {
                block: {
                    prismTheme: rootTheme.codeBlock.prismTheme,
                },
                inline: {
                    color: rootTheme.codeInline.color,
                    background: rootTheme.codeInline.backgroundColor,
                    font: {
                        size: rootTheme.paragraphText.fontSize,
                    }
                },
            },
            blockquote: {
                font: {
                    family: rootTheme.tertiaryText.fontFamily,
                    weight: rootTheme.tertiaryText.fontWeight,
                    size: 'inherit',
                    color: rootTheme.tokens.color.black,

                },
                border: {
                    left: {
                        style: 'solid',
                        color: rootTheme.highlightText.borderColor,
                        width: '0.5rem',
                    }
                },
                backgroundColor: rootTheme.highlightText.backgroundColor,
            },
            img: {
                border: {
                    width: '1px',
                    radius: 0,
                    color: f36Tokens.gray400,
                    style: 'none',
                }
            },
            h2: {
                font: {
                    family: rootTheme.displayText.fontFamily,
                    weight: rootTheme.paragraphText.fontWeight,
                    size: f36Tokens.fontSize2Xl,
                },
                color: rootTheme.displayText.color,
                opacity: 1,
                lineHeight: f36Tokens.lineHeight2Xl,
                margin: {
                    bottom: '1rem'
                }
            },
            h3: {
                font: {
                    family: rootTheme.displayText.fontFamily,
                    weight: rootTheme.paragraphText.fontWeight,
                    size: f36Tokens.fontSizeXl,
                },
                color: rootTheme.displayText.color,
                opacity: 1,
                lineHeight: f36Tokens.lineHeight2Xl,
                margin: {
                    bottom: '0.75rem'
                }
            },
            table: {
                border: {
                    style: 'dotted',
                    radius: 0,
                    width: '1px',
                    color: rootTheme.secondaryText.color,
                },
                headerCell: {
                    font: {
                        family: rootTheme.displayText.fontFamily,
                        weight: rootTheme.secondaryText.fontWeight,
                    }
                },
                cell: {
                    color: rootTheme.paragraphText.color,
                    font: {
                        family: rootTheme.paragraphText.fontFamily,
                        weight: rootTheme.paragraphText.fontWeight,
                    }
                }
            },
        },
    },
    definition: {
        term: {
            color: rootTheme.displayText.color,
            opacity: 1,
            font: {
                family: rootTheme.paragraphText.fontFamily,
                weight: rootTheme.paragraphText.fontWeight,
                size: f36Tokens.fontSizeL,
            },
            textDecoration: {
                style: 'dotted',
                color: f36Tokens.green500,
                width: '2px',
            },
        },
        tooltip: {
            color: f36Tokens.gray900,
            backgroundColor: f36Tokens.gray100,
            font: {
                family: rootTheme.paragraphText.fontFamily,
                weight: rootTheme.paragraphText.fontWeight,
            },
            border: {
                radius: 0,
                width: '2px',
                style: 'solid',
                color: f36Tokens.gray500,
            },
        },
    }
})

