import f36Tokens from '@contentful/f36-tokens'

export interface UnitText {
    color: string,
    fontFamily: string,
    fontWeight: number,
}

export interface RootThemeTokens {
    fontFamily: {
        monospace: string,
        sanSerif: string,
    },
    color: {
        green: string,
        punctuation: string,
        comment: string,
        property: string,
        cyan: string,
        orange: string,
        black: string,
        lightBlack: string
        white: string,
        link: string
    }
}

export interface RootTheme {
    tokens: RootThemeTokens,
    global: {
        color: string,
        articleMaxWidth: string,
        backgroundColor: string,
        textColor: string,
    },
    displayText: UnitText,
    secondaryText: UnitText,
    tertiaryText: UnitText & { backgroundColor: string }
    paragraphText: UnitText & { fontSize: string, lineHeight: string }
    captionText: UnitText & {
        fontSize: string,
        borderColor: string,
        borderStyle: string
    },
    highlightText: UnitText & {
        backgroundColor: string,
        borderColor: string,
        borderStyle: string,
        borderRadius: string,
    },
    disableText: UnitText & { backgroundColor: string },
    codeBlock: {
        prismTheme: string
    },
    codeInline: UnitText & { backgroundColor: string },
    mermaidDiagram: {
        theme: string,
        themeVariables: {
            fontFamily: string
        },
    }
}


export const tokens: RootThemeTokens = {
    fontFamily: {
        monospace: 'Ubuntu Mono, monospace;',
        sanSerif: '-apple-system,BlinkMacSystemFont,Segoe UI,Helvetica,Arial,sans-serif,Apple Color Emoji,Segoe UI Emoji,Segoe UI Symbol !important;',
    },
    color: {
        green: '#7ec699',
        punctuation: '#ccc',
        comment: '#999',
        property: '#f8c555',
        orange: '#f08d49',
        cyan: '#67cdcc',
        black: '#0C141C',
        lightBlack: '#222222',
        white: '#ffffff',
        link: '#cc99cd',
    }
}

export const darkTheme: RootTheme = {
    tokens,
    global: {
        color: '#222222',
        articleMaxWidth: '1000px',
        backgroundColor: f36Tokens.colorBlack,
        textColor: tokens.color.white,
    },
    // headings
    displayText: {
        color: tokens.color.white,
        fontFamily: tokens.fontFamily.sanSerif,
        fontWeight: 900,
    },
    // subheadings, etc
    secondaryText: {
        color: '#f08d49',
        fontFamily: tokens.fontFamily.sanSerif,
        fontWeight: 300,
    },
    // block labels, etc
    tertiaryText: {
        color: tokens.color.black,
        fontFamily: tokens.fontFamily.monospace,
        fontWeight: 400,
        backgroundColor: tokens.color.green,
    },
    paragraphText: {
        color: tokens.color.white,
        fontFamily: tokens.fontFamily.sanSerif,
        fontWeight: 400,
        fontSize: f36Tokens.fontSizeL,
        lineHeight: f36Tokens.lineHeightL
    },
    captionText: {
        color: tokens.color.punctuation,
        fontFamily: tokens.fontFamily.monospace,
        fontWeight: 400,
        fontSize: f36Tokens.fontSizeM,
        borderColor: f36Tokens.gray200,
        borderStyle: 'solid',
    },
    highlightText: {
        color: tokens.color.black,
        fontFamily: tokens.fontFamily.monospace,
        fontWeight: 400,
        backgroundColor: tokens.color.cyan,
        borderColor: f36Tokens.green900,
        borderStyle: 'dotted',
        borderRadius: '0px',
    },
    // dark theme
    disableText: {
        color: tokens.color.property,
        fontFamily: tokens.fontFamily.monospace,
        fontWeight: 400,
        backgroundColor: tokens.color.punctuation
    },
    codeBlock: {
        prismTheme: 'prism-tomorrow.min.css'
    },
    codeInline: {
        color: '#383838',
        fontFamily: tokens.fontFamily.monospace,
        fontWeight: 400,
        backgroundColor: '#f0f0f0',
    },
    mermaidDiagram: {
        theme: 'dark',
        themeVariables: {
            fontFamily: 'monospace'
        },
    }
}

