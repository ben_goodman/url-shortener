import { expandedStylesGenerator } from './globalStylesGenerator'
import { darkTheme } from './rootThemes'

const themes = {
    default: expandedStylesGenerator(darkTheme),
}

export default themes