module "dns_alias" {
    source  = "gitlab.com/ben_goodman/s3-website/aws//modules/dns"
    version = "2.0.0"

    hosted_zone_name       = var.hosted_zone_name
    cloudfront_domain_name = module.cloudfront.cloudfront_default_domain
    domain_name            = var.website_aliases
}