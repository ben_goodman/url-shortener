output "bucket_name" {
    value = module.cloudfront.bucket_name
}

output "cloudfront_id" {
    value = module.cloudfront.cloudfront_id
}

output "user_pool_client_id" {
    value = aws_cognito_user_pool_client.default_client.id
}
