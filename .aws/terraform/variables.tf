variable "project_name" {}

variable "resource_namespace" {}

variable "dynamo_table_name" {}

variable "acm_certificate_arn" {}

variable "website_aliases" {}

variable "hosted_zone_name" {}