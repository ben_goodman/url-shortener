import type { APIGatewayProxyEventV2, APIGatewayProxyResultV2 } from 'aws-lambda'
import queryString from 'query-string'
import {
    createItem,
    deleteItem,
    getItem,
    scanItems
} from './items'

export const handler = async (event: APIGatewayProxyEventV2): Promise<APIGatewayProxyResultV2> => {
    const method = event.requestContext.http.method
    const paths = event.rawPath.split('/').filter(x => x !== '')
    const {
        limit = undefined,
        from = undefined,
    } = queryString.parse(event.rawQueryString, {parseNumbers: true})

    const body = event.body && event.isBase64Encoded ? Buffer.from(event.body!, 'base64').toString('utf-8') : event.body

    // test the api
    // GET /api/health
    if (method === 'GET' && paths[1] === 'health') {
        return {
            headers: {"Content-Type": "application/json"},
            statusCode: 200,
            body: JSON.stringify({ message: 'Ok' }),
        }
    }

    // create a new item
    // PUT /api/items
    if (method === 'PUT' && paths[1] === 'items') {
        return createItem(body)
    }

    // delete an item
    // DELETE /api/items/{id}
    if (method === 'DELETE' && paths[1] === 'items' && paths.length === 3) {
        return deleteItem(paths[2])
    }

    // get a specific item
    // GET /api/items/{id}
    if (method === 'GET' && paths[1] === 'items' && paths.length === 3) {
        return getItem(paths[2])
    }

    // get a paged scan of all items
    // GET /api/items?limit={number=10}&from={number=0}
    if (method === 'GET' && paths[1] === 'items') {
        return scanItems({
            limit: limit as number|undefined,
            from: from as string|undefined
        })
    }

    // default response
    return {
        headers: {"Content-Type": "application/json"},
        statusCode: 400,
        body: JSON.stringify({ message: 'Unsupported route', method, paths, event }),
    }
}
