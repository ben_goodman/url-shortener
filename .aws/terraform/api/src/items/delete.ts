import { DynamoDBClient } from "@aws-sdk/client-dynamodb";
import { DeleteCommand, DynamoDBDocumentClient } from "@aws-sdk/lib-dynamodb";
import { APIGatewayProxyResultV2 } from "aws-lambda";

const client = new DynamoDBClient({})
const dynamo = DynamoDBDocumentClient.from(client)
const TABLE_NAME = process.env.TABLE_NAME

const headers = {
    "Content-Type": "application/json",
}

export const deleteItem = async (id: string): Promise<APIGatewayProxyResultV2> => {
    await dynamo.send(
        new DeleteCommand({
            TableName: TABLE_NAME,
            Key: {
                id,
            },
        })
    ).catch((err) => {
        console.log(`deleting item ${id} failed`)
        console.log(err)
        throw err
    })


    return {
        headers,
        statusCode: 200,
        body: JSON.stringify({ message: 'Item deleted' }),
    }
}