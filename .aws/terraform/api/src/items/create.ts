import { type APIGatewayProxyResultV2 } from "aws-lambda"
import { DynamoDBClient } from "@aws-sdk/client-dynamodb";
import { DynamoDBDocumentClient, PutCommand } from "@aws-sdk/lib-dynamodb";

const client = new DynamoDBClient({})
const dynamo = DynamoDBDocumentClient.from(client)
const TABLE_NAME = process.env.TABLE_NAME

const headers = {"Content-Type": "application/json"}

export const createItem = async (body?: string): Promise<APIGatewayProxyResultV2> => {
    if (body === undefined) {
        return {
            headers,
            statusCode: 400,
            body: JSON.stringify({ message: 'No body provided' }),
        }
    }

    try {
        const {id, targetUrl} = JSON.parse(body)

        await dynamo.send(
            new PutCommand({
                TableName: TABLE_NAME,
                Item: {
                    id,
                    targetUrl,
                    dateUploaded: new Date().toISOString()
                },
            })
        ).catch((err) => {
            console.log(`adding URL ${id} to dynamo failed`)
            console.log(err)
            throw err
        })

        return {
            headers,
            statusCode: 200,
            body: JSON.stringify({ message: 'URL added.', id, targetUrl}),
        }
    } catch (error) {
        return {
            headers,
            statusCode: 500,
            body: JSON.stringify({ message: 'Failed to add URL.', err: (error as any).errorMessage }),
        }
    }
}
