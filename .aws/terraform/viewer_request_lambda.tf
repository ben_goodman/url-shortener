# locals {
#     viewer_source_code_dir = "${path.module}/lambda_authorizer"
# }

# resource "null_resource" "viewer_request_handler_source_builder" {
#     provisioner "local-exec" {
#         working_dir = local.viewer_source_code_dir
#         command     = "make all"
#         environment = {
#             USER_POOL_ID  = aws_cognito_user_pool.default_pool.id
#             APP_CLIENT_ID = aws_cognito_user_pool_client.default_client.id
#         }
#     }
#     triggers = {
#         always_run = "${timestamp()}"
#     }
# }

# data "null_data_source" "wait_for_api_viewer_request_handler" {
#     inputs = {
#         lambda_exporter_id = "${null_resource.viewer_request_handler_source_builder.id}"
#         source_file        = "${local.viewer_source_code_dir}/dist/index.js"
#     }
# }

# data "archive_file" "viewer_request_handler_build_payload" {
#     depends_on       = [ data.null_data_source.wait_for_api_viewer_request_handler ]
#     type             = "zip"
#     source_file      = data.null_data_source.wait_for_api_viewer_request_handler.outputs.source_file
#     output_file_mode = "0666"
#     output_path      = "${local.viewer_source_code_dir}/dist/index.js.zip"
# }

module "viewer_request_payload" {
    source  = "gitlab.com/ben_goodman/lambda-function/aws//modules/code_builder"
    version = "3.0.0"

    output_filename = "index.js"
    source_root     = "${path.module}/lambda_authorizer"
    build_command   = "make all"

    environment_variables = {
        USER_POOL_ID  = aws_cognito_user_pool.default_pool.id
        APP_CLIENT_ID = aws_cognito_user_pool_client.default_client.id
    }
}


module "viewer_request_lambda" {
    source  = "gitlab.com/ben_goodman/lambda-function/aws"
    version = "2.0.0"

    org              = var.resource_namespace
    project_name     = var.project_name
    lambda_payload   = module.viewer_request_payload.archive_file
    function_name    = "url-shortener-cf-req-${random_id.cd_function_suffix.hex}"
    function_handler = "index.handler"
    publish          = true
    memory_size      = 128
}

