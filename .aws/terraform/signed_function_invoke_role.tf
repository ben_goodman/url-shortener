# Creates a role that allows an authorizing lambda@edge function
# to invoke another lambda function's secured URL.

# This policy document allows the lambda function to be invoked by cloudfront.
data "aws_iam_policy_document" "edge_lambda_execution_role" {
    statement {
    effect = "Allow"
    principals {
        type        = "Service"
        identifiers = [
            "lambda.amazonaws.com",
            "edgelambda.amazonaws.com"
        ]
    }
        actions = ["sts:AssumeRole"]
    }
}

#  We will pass this role to the authorizing edge function
resource "aws_iam_role" "signed_auth_iam_role" {
    name = "edge-lambda-exe-${random_id.cd_function_suffix.hex}"
    assume_role_policy = data.aws_iam_policy_document.edge_lambda_execution_role.json
}

resource "aws_lambda_permission" "allow_cloudfront_execution" {
    function_name = module.signed_authentication_lambda.name
    statement_id  = "allow-cf-exec-${random_id.cd_function_suffix.hex}"
    action        = "lambda:GetFunction"
    principal     = "edgelambda.amazonaws.com"
}

# gives permission to invoke the secured function.
resource "aws_iam_policy" "lambda_invoke_function_policy" {
  name = "invoke-function-url-policy-${random_id.cd_function_suffix.hex}"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect = "Allow"
        Action = [
            "lambda:InvokeFunctionUrl"
        ]
        Resource = [
            # This is the secure lambda function
            module.api_lambda.arn
        ]
      }
    ]
  })
}

# Attaches the secure invocation to the authorizer's role
resource "aws_iam_role_policy_attachment" "lambda_invoke_signed_function_attachment" {
  role       = aws_iam_role.signed_auth_iam_role.name
  policy_arn = aws_iam_policy.lambda_invoke_function_policy.arn
}