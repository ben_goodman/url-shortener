import { DynamoDBClient } from '@aws-sdk/client-dynamodb'
import { DynamoDBDocumentClient, GetCommand } from '@aws-sdk/lib-dynamodb'
import { type Handler, type CloudFrontRequestEvent } from 'aws-lambda'


const client = new DynamoDBClient({})
const dynamo = DynamoDBDocumentClient.from(client)
const TABLE_NAME = process.env.TABLE_NAME

// attached to the default route
// if path if for a short link
//  1. lookup long link
//  2. redirect to the long link
export const handler: Handler<CloudFrontRequestEvent> = async (event) => {
    const request = event.Records[0].cf.request;

    try {
        // ['', shortPath, ...rest]
        const paths = request.uri.split('/')

        if (request.uri === '/') {
            // passthrough for index page
            return request
        }

        const shortPath = paths[1]

        // a short path is one that does not end with a file extension
        if (paths.length === 2 && !shortPath.includes('.')) {
            // lookup long link
            const response = await dynamo.send(
                new GetCommand({
                    TableName: TABLE_NAME,
                    Key: {
                        id: shortPath,
                    },
                })
            );

            const targetUrl = response.Item?.targetUrl

            if (!targetUrl) {
                return {
                    status: 404,
                    statusDescription: "Not Found",
                    headers: {
                        "content-type": [
                            {
                                key: "Content-Type",
                                value: "application/json",
                            },
                        ],
                    },
                    body: JSON.stringify({ message: "Not Found" })
                }
            }

            return {
                status: '302',
                statusDescription: 'Found',
                headers: {
                    location: [{
                        key: 'Location',
                        value: targetUrl,
                    }],
                },
            }
        } else {
            // passthrough to origin
            return request
        }
    } catch (error) {
        const response = {
            status: 500,
            statusDescription: "Internal Server Error",
            headers: {
                "content-type": [
                    {
                        key: "Content-Type",
                        value: "application/json",
                    },
                ],
            },
            body: JSON.stringify(event, null, 2)
        };
        return response;
    }
}
