resource_namespace  = "bgoodman"
project_name        = "url-shortener"
dynamo_table_name   = "url-shortener"
acm_certificate_arn = "arn:aws:acm:us-east-1:757687723154:certificate/e8deff76-1446-43be-9063-85b5ea8638d6"
website_aliases     = ["unimportant.link", "www.unimportant.link"]
hosted_zone_name    = "unimportant.link"