module "api_handler_payload" {
    source  = "gitlab.com/ben_goodman/lambda-function/aws//modules/code_builder"
    version = "3.0.0"

    output_filename = "index.js"
    source_root     = "${path.module}/api"
    build_command   = "make all"
}

module "api_lambda" {
    source  = "gitlab.com/ben_goodman/lambda-function/aws"
    version = "3.0.0"

    org              = var.resource_namespace
    project_name     = var.project_name
    lambda_payload   = module.api_handler_payload.archive_file
    function_name    = "url-${random_id.cd_function_suffix.hex}"
    function_handler = "index.handler"
    publish          = true
    memory_size      = 512
    role             = aws_iam_role.dynamo_db_lambda_role
    timeout          = 5

    environment_variables = {
        TABLE_NAME = aws_dynamodb_table.default.name
    }
}

resource "aws_lambda_function_url" "api_lambda_endpoint" {
    function_name      = module.api_lambda.name
    authorization_type = "AWS_IAM"
}
