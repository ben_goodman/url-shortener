module "viewer_redirect_payload" {
    source  = "gitlab.com/ben_goodman/lambda-function/aws//modules/code_builder"
    version = "3.0.0"

    output_filename = "index.js"
    source_root     = "${path.module}/lambda_viewer_redirect"
    build_command   = "make all"

    # Note: Lambda@Edge functions do not support environment variables.
    # Therefore, they are baked into the function code.
    environment_variables = {
        TABLE_NAME = aws_dynamodb_table.default.name
    }
}


module "viewer_redirect_lambda" {
    source  = "gitlab.com/ben_goodman/lambda-function/aws"
    version = "2.0.0"

    org              = var.resource_namespace
    project_name     = var.project_name
    lambda_payload   = module.viewer_redirect_payload.archive_file
    function_name    = "redirecter-${random_id.cd_function_suffix.hex}"
    function_handler = "index.handler"
    publish          = true
    memory_size      = 128
    role             = aws_iam_role.dynamo_db_lambda_role
}
