# URL Shortener

This is a simple URL shortener that uses AWS Lambda and DynamoDB  to create a serverless API that will redirect to the original URL.

![](./docs/url-shortener-architecture.png)

## Deployment

1. Clone the repository
1. Confirm the values in `.aws/terraform/terraform.tfvars`.
1. From `.aws/terraform`, run `terraform plan` and `terraform apply`. This will create the necessary resources in AWS.
1. From the root of the repository, run `npm install && npm start` to install the necessary UI dependencies and launch the UI locally.

## Usage

Using the AWS CLI, create a new Cognito user.

```bash
aws cognito-idp admin-set-user-password \
  --user-pool-id 'us-east-1_xxxxxx' \
  --username 'example' \
  --password 'password' \
  --permanent
  ```

Using the UI, sign in with the new user and create a new short URL.